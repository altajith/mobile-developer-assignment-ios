//
//  DropDownButton.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class DropDownButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    func initUI() {
        setTitleColor(.darkGray, for: .normal)
        setTitleColor(.gray, for: .highlighted)
        contentHorizontalAlignment = .left
        titleLabel!.font = Functions.font(size: 16)
        setImage(UIImage(named: "down-arrow"), for: .normal)
        
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - 35), bottom: 0, right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}
