//
//  AppCommonButton.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class AppCommonButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    func initUI() {
        backgroundColor = .systemBlue
        setTitleColor(.white, for: .normal)
        
        layer.masksToBounds = true
        layer.cornerRadius = 15
        layer.borderWidth = 2
        layer.borderColor = UIColor.darkGray.cgColor
    }
    
    override var isHighlighted: Bool {
        didSet {
            if layer.opacity > 0 {
                layer.opacity = isHighlighted ? 0.7 : 1
            }
            
            if isHighlighted {
                let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
                colorAnimation.fromValue = UIColor.systemBlue.cgColor
                colorAnimation.duration = 0.5
                layer.add(colorAnimation, forKey: "ColorPulse")
            }
        }
    }
}
