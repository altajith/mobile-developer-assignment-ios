//
//  Functions.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class Functions {
    
    public static var contentTypes = ["Image", "Video", "Pdf", "Others"]
    
    public static func font(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica Neue", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    public static func alert(vc: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    public static func verifyUrl(urlString: String) -> Bool {
        if let url = NSURL(string: urlString) {
            // check if your application can open the NSURL instance
            return UIApplication.shared.canOpenURL(url as URL)
        }
        return false
    }
    
    public static func calcHeightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}
