//
//  TitleBar.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class TitleBar: UIView {
    
    public var title: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.font = Functions.font(size: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    public var close: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "close"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    public var line: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private var closeActionListner: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    private func initUI() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        addSubview(title)
        addSubview(close)
        addSubview(line)
        
        close.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        initLayout()
    }
    
    private func initLayout() {
        NSLayoutConstraint.activate([
            //Title Label
            title.topAnchor.constraint(equalTo: topAnchor),
            title.leadingAnchor.constraint(equalTo: leadingAnchor),
            title.trailingAnchor.constraint(equalTo: trailingAnchor),
            title.heightAnchor.constraint(equalToConstant: 40),
            
            //Close Button
            close.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            close.leadingAnchor.constraint(equalTo: title.trailingAnchor, constant: -20),
            close.heightAnchor.constraint(equalToConstant: 16),
            close.widthAnchor.constraint(equalToConstant: 16),
            
            //Line
            line.topAnchor.constraint(equalTo: bottomAnchor),
            line.leadingAnchor.constraint(equalTo: leadingAnchor),
            line.trailingAnchor.constraint(equalTo: trailingAnchor),
            line.heightAnchor.constraint(equalToConstant: 4),
            
            //Self
            heightAnchor.constraint(equalToConstant: 44)
        ])
    }
    
    @objc func closeAction(button: UIButton) {
        closeActionListner!()
    }
    
    public func onCloseAction(action: @escaping () -> Void) {
        closeActionListner = action
    }
}
