//
//  Download.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit
import Alamofire

protocol DownloadDelegate: class {
    //this will be triggered after file is downloaded,
    //if we want to validate the content type then we can use mime to do that.
    func downloadedData(fileName: String, mimeType: String)
    
    //if download file throw errors
    func downloadedError(error: String)
    
    //update the progress of the file download
    func onProgress(progress: Double)
}

class Download {
    
    weak var delegate: DownloadDelegate?
    
    let session = URLSession.shared
    var fileUrl: URL!
    
    //set up the downloadable url
    func setUrl(url: String) {
        fileUrl = URL(string: url)
    }
    
    //start the downloading process
    func start() {
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory, in:  .userDomainMask)
        Alamofire.download(fileUrl, to: destination)
        .downloadProgress { progress in
            //update the downloading progress
            self.delegate?.onProgress(progress: progress.fractionCompleted)
        }
        .response(completionHandler: { (downloadResponse) in
            
            let localFileName = (downloadResponse.destinationURL?.path.fileName())! + "." + (downloadResponse.destinationURL?.path.fileExtension())!
            
            if (downloadResponse.error != nil) {
                //if file is already downloaded it will throw an error but the destinationURL will have the path to the existing file
                if downloadResponse.destinationURL != nil && !(downloadResponse.destinationURL?.path.isEmpty)! {
                    self.delegate?.downloadedData(fileName: localFileName, mimeType: (downloadResponse.response?.mimeType)!)
                } else {
                    //pass the error
                    self.delegate?.downloadedError(error: downloadResponse.error!.localizedDescription)
                }
            } else {
                self.delegate?.downloadedData(fileName: localFileName, mimeType: (downloadResponse.response?.mimeType)!)
            }
        })
    }
}
