//
//  AppCommonTextView.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class AppCommonTextView: UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    func initUI() {
        font = Functions.font(size: 16)
        textColor = .darkGray
        backgroundColor = .white
        
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1
    }
}
