//
//  AppCommonTextField.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class AppCommonTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    func initUI() {
        font = Functions.font(size: 16)
        textColor = .darkGray
        
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10.0, height: bounds.height))
        leftViewMode = .always
        
        rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10.0, height: bounds.height))
        rightViewMode = .always
        
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1
    }
}
