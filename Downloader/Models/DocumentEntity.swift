//
//  DocumentEntity.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DocumentEntity {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private var uuid: UUID = UUID()
    private var url: String = ""
    private var desc: String = ""
    private var contentType: String = ""
    private var fileName: String = ""
    private var createdAt: Date = Date()
    
    @objc public func getUUID() -> UUID {
        return self.uuid
    }
    
    @objc public func getUrl() -> String {
        return self.url
    }
    
    @objc public func getDescription() -> String {
        return self.desc
    }
    
    @objc public func getContentType() -> String {
        return self.contentType
    }
    
    @objc public func getFileName() -> String {
        return self.fileName
    }
    
    @objc public func getCreatedAt() -> Date {
        return self.createdAt
    }
    
    @objc public func setUUID(value: UUID) {
        self.uuid = value
    }
    
    @objc public func setUrl(value: String) {
        self.url = value
    }
    
    @objc public func setDescription(value: String) {
        self.desc = value
    }
    
    @objc public func setContentType(value: String) {
        self.contentType = value
    }
    
    @objc public func setFileName(value: String) {
        self.fileName = value
    }
    
    @objc public func setCreatedAt(value: Date) {
        self.createdAt = value
    }
    
    public func getAll() -> Array<DocumentEntity> {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Document")
        let sort = NSSortDescriptor(key: #keyPath(Document.created_at), ascending: false)
        request.sortDescriptors = [sort]
        var items: Array<DocumentEntity> = Array<DocumentEntity>()
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let de = DocumentEntity()
                if let uuid = data.value(forKey: "uuid") as? UUID {
                    de.setUUID(value: uuid)
                }
                if let url = data.value(forKey: "url") as? String {
                    de.setUrl(value: url)
                }
                if let desc = data.value(forKey: "desc") as? String {
                    de.setDescription(value: desc)
                }
                if let contentType = data.value(forKey: "content_type") as? String {
                    de.setContentType(value: contentType)
                }
                if let fileName = data.value(forKey: "file_name") as? String {
                    de.setFileName(value: fileName)
                }
                if let createdAt = data.value(forKey: "created_at") as? Date {
                    de.setCreatedAt(value: createdAt)
                }
                items.append(de)
            }
        } catch {
            print("Failed to retrieve the documents")
        }
        return items
    }
    
    @objc public func get() -> Bool {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Document")
        request.predicate = NSPredicate(format: "uuid = %@", self.getUUID() as CVarArg)
        request.returnsObjectsAsFaults = false
        request.fetchLimit = 1
        var exists: Bool = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let url = data.value(forKey: "url") as? String {
                    self.setUrl(value: url)
                }
                if let desc = data.value(forKey: "desc") as? String {
                    self.setDescription(value: desc)
                }
                if let contentType = data.value(forKey: "content_type") as? String {
                    self.setContentType(value: contentType)
                }
                if let fileName = data.value(forKey: "file_name") as? String {
                    self.setFileName(value: fileName)
                }
                if let createdAt = data.value(forKey: "created_at") as? Date {
                    self.setCreatedAt(value: createdAt)
                }
                exists = true
                break
            }
        } catch {
            print("Failed to retrieve the document")
        }
        return exists
    }
    
    @objc public func update() {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Document")
        request.predicate = NSPredicate(format: "uuid = %@", self.getUUID() as CVarArg)
        request.returnsObjectsAsFaults = false
        request.fetchLimit = 1
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                data.setValue(self.getUrl(), forKey: "url")
                data.setValue(self.getDescription(), forKey: "desc")
                data.setValue(self.getContentType(), forKey: "content_type")
                data.setValue(self.getFileName(), forKey: "file_name")
                do {
                    try context.save()
                } catch {
                    print("Failed saving document")
                }
                break
            }
        } catch {
            print("Failed to retrieve the document")
        }
    }
    
    @objc public func create() {
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Document", in: context)
        let newDocument = NSManagedObject(entity: entity!, insertInto: context)
        newDocument.setValue(self.getUUID(), forKey: "uuid")
        newDocument.setValue(self.getUrl(), forKey: "url")
        newDocument.setValue(self.getDescription(), forKey: "desc")
        newDocument.setValue(self.getContentType(), forKey: "content_type")
        newDocument.setValue(self.getFileName(), forKey: "file_name")
        newDocument.setValue(self.getCreatedAt(), forKey: "created_at")
        do {
            try context.save()
        } catch {
            print("Failed saving the document")
        }
    }
    
    public func delete() -> Bool {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Document")
        request.predicate = NSPredicate(format: "uuid = %@", self.getUUID() as CVarArg)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        do {
            try context.execute(deleteRequest)
            return true
        } catch _ as NSError {
            return false
        }
    }
}
