//
//  DownloadController.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//


import UIKit
import DropDown
import JGProgressHUD

class DownloadController: UIViewController, DownloadDelegate, UITextViewDelegate {
    
    var titleBar: TitleBar = {
        let bar = TitleBar()
        bar.title.text = "Download File"
        return bar
    }()
    
    var urlLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 16)
        lbl.textColor = .darkGray
        lbl.text = "URL"
        return lbl
    }()
    
    var urlTextField: AppCommonTextField = {
        let textField = AppCommonTextField()
        textField.attributedPlaceholder = NSAttributedString(string: "http://", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textField.keyboardType = .URL
        textField.autocorrectionType = .no
        return textField
    }()
    
    var descLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 16)
        lbl.textColor = .darkGray
        lbl.text = "Description"
        return lbl
    }()
    
    var descTextField: AppCommonTextView = {
        let textView = AppCommonTextView()
        textView.autocorrectionType = .no
        textView.textContainer.lineBreakMode = .byWordWrapping
        return textView
    }()
    
    var contentTypeLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 16)
        lbl.textColor = .darkGray
        lbl.text = "Content Type"
        return lbl
    }()
    
    var selectedContentType: String = Functions.contentTypes[0]
    
    var contentTypeButton: DropDownButton = {
        let btn = DropDownButton()
        btn.setTitle(Functions.contentTypes[0], for: .normal)
        return btn
    }()
    
    let dropDown = DropDown()
    
    var downloadButton: AppCommonButton = {
        let btn = AppCommonButton()
        btn.setTitle("Download", for: .normal)
        btn.titleLabel?.font = Functions.font(size: 16)
        return btn
    }()
    
    let download = Download()
    var document: DocumentEntity!
    let loading = JGProgressHUD(style: .dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        DropDown.appearance().textFont = Functions.font(size: 16)
        dropDown.anchorView = contentTypeButton
        dropDown.dataSource = Functions.contentTypes
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.selectedContentType = Functions.contentTypes[index]
            self.contentTypeButton.setTitle(self.selectedContentType, for: .normal)
        }
    
        view.addSubview(titleBar)
        titleBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        titleBar.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        titleBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        titleBar.onCloseAction {
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        
        view.addSubview(urlLabel)
        urlLabel.translatesAutoresizingMaskIntoConstraints = false
        urlLabel.topAnchor.constraint(equalTo: titleBar.bottomAnchor, constant: 20).isActive = true
        urlLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        urlLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        urlLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(urlTextField)
        urlTextField.translatesAutoresizingMaskIntoConstraints = false
        urlTextField.topAnchor.constraint(equalTo: urlLabel.bottomAnchor, constant: 5).isActive = true
        urlTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        urlTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        urlTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        view.addSubview(descLabel)
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        descLabel.topAnchor.constraint(equalTo: urlTextField.bottomAnchor, constant: 10).isActive = true
        descLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        descLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        descLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(descTextField)
        descTextField.translatesAutoresizingMaskIntoConstraints = false
        descTextField.topAnchor.constraint(equalTo: descLabel.bottomAnchor, constant: 5).isActive = true
        descTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        descTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        descTextField.heightAnchor.constraint(equalToConstant: 60).isActive = true
        descTextField.delegate = self
        
        view.addSubview(contentTypeLabel)
        contentTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        contentTypeLabel.topAnchor.constraint(equalTo: descTextField.bottomAnchor, constant: 10).isActive = true
        contentTypeLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        contentTypeLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        contentTypeLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.addSubview(contentTypeButton)
        contentTypeButton.translatesAutoresizingMaskIntoConstraints = false
        contentTypeButton.topAnchor.constraint(equalTo: contentTypeLabel.bottomAnchor, constant: 5).isActive = true
        contentTypeButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        contentTypeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        contentTypeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        contentTypeButton.addTarget(self, action: #selector(selectContentTypeAction), for: .touchUpInside)
        
        view.addSubview(downloadButton)
        downloadButton.translatesAutoresizingMaskIntoConstraints = false
        downloadButton.topAnchor.constraint(equalTo: contentTypeButton.bottomAnchor, constant: 30).isActive = true
        downloadButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        downloadButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        downloadButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        downloadButton.addTarget(self, action: #selector(downloadAction), for: .touchUpInside)
        
        download.delegate = self
        document = DocumentEntity()
        
        //Progress bar settings
        loading.textLabel.text = "Downloading"
        loading.indicatorView = JGProgressHUDPieIndicatorView()
        
        //Hide keyboard if you tap anywhere on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func selectContentTypeAction(button: UIButton) {
        dropDown.show()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 100;
    } 
    
    @objc func downloadAction(button: AppCommonButton) {
        var validated = true
        if urlTextField.text!.isEmpty {
            validated = false
            Functions.alert(vc: self, title: "Required", message: "Please enter the URL")
        } else if !Functions.verifyUrl(urlString: urlTextField.text!) {
            validated = false
            Functions.alert(vc: self, title: "Invalid", message: "Please enter a valid URL")
        } else if descTextField.text!.isEmpty {
            validated = false
            Functions.alert(vc: self, title: "Required", message: "Please enter the Description")
        } else if descTextField.text!.count > 100 { //safe validation
            validated = false
            Functions.alert(vc: self, title: "Limit Reached", message: "Description is limited to 100 characters.")
        }
        if validated {
            document = DocumentEntity()
            document.setUrl(value: urlTextField.text!)
            document.setDescription(value: descTextField.text!)
            document.setContentType(value: selectedContentType)
            download.setUrl(url: document.getUrl())
            download.start()
            loading.show(in: view)
        }
    }
    
    //Download completion delegate function
    func downloadedData(fileName: String, mimeType: String) {
        document.setFileName(value: fileName)
        document.create()
        DispatchQueue.main.async {
            self.loading.textLabel.text = "Completed!"
            self.loading.dismiss(afterDelay: 1.0, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //Download error delegate function
    func downloadedError(error: String) {
        DispatchQueue.main.async {
            self.loading.dismiss(animated: true)
            Functions.alert(vc: self, title: "Error", message: error)
        }
    }
    
    //Download on progress delegate function
    func onProgress(progress: Double) {
        print("\(progress) progress")
        DispatchQueue.main.async {
            self.loading.progress = Float(progress)
        }
    }
}
