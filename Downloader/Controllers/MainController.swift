//
//  MainController.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class MainController: UIViewController {

    @IBOutlet weak var downloadFileButton: AppCommonButton!
    @IBOutlet weak var viewListButton: AppCommonButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        
        downloadFileButton.translatesAutoresizingMaskIntoConstraints = false
        viewListButton.translatesAutoresizingMaskIntoConstraints = false

        downloadFileButton.topAnchor.constraint(equalTo: view.centerYAnchor, constant: -50).isActive = true
        downloadFileButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        downloadFileButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        downloadFileButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        viewListButton.topAnchor.constraint(equalTo: downloadFileButton.bottomAnchor, constant: 10).isActive = true
        viewListButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        viewListButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        viewListButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @IBAction func downloadFileAction(_ sender: AppCommonButton) {
        let dC = DownloadController()
        dC.modalPresentationStyle = .fullScreen
        self.present(dC, animated: true)
    }
    
    @IBAction func viewListAction(_ sender: AppCommonButton) {
        let vlC = ViewListController()
        vlC.modalPresentationStyle = .fullScreen
        self.present(vlC, animated: true)
    }
    
}

