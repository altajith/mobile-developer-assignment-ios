//
//  ViewListController.swift
//  Downloader
//
//  Created by AJ on 1/9/20.
//  Copyright © 2020 AJ. All rights reserved.
//


import UIKit
import QuickLook
import Alamofire

class ViewListController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, QLPreviewControllerDataSource {
   
    var titleBar: TitleBar = {
        let bar = TitleBar()
        bar.title.text = "Downloaded Files"
        return bar
    }()
    
    var documentsCollectionView: UICollectionView!
    var documents: Array<DocumentEntity> = Array<DocumentEntity>()
    var previewItem: URL!
    var layoutWidth: CGFloat!
    let layoutHegith: CGFloat = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        layoutWidth = view.frame.width - 25
        
        //Retrieve all the documents from core data
        documents = DocumentEntity().getAll()
    
        view.addSubview(titleBar)
        titleBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        titleBar.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        titleBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        titleBar.onCloseAction {
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        
        //Setting up the initial layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: layoutWidth, height: layoutHegith)
        
        documentsCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.addSubview(documentsCollectionView)
        documentsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        documentsCollectionView.topAnchor.constraint(equalTo: titleBar.bottomAnchor, constant: 10).isActive = true
        documentsCollectionView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -25).isActive = true
        documentsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        documentsCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor, constant: -80).isActive = true
        documentsCollectionView.dataSource = self
        documentsCollectionView.delegate = self
        documentsCollectionView.backgroundColor = .white
        documentsCollectionView.showsVerticalScrollIndicator = false
        documentsCollectionView.register(DocumentCell.self, forCellWithReuseIdentifier: "docCell")
    }
     
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documents.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let docCell = collectionView.dequeueReusableCell(withReuseIdentifier: "docCell", for: indexPath) as! DocumentCell
        let index = indexPath.row
        let desc = documents[index].getDescription()
        docCell.typeLabel.text = documents[index].getContentType()
        docCell.descLabel.text = desc
        docCell.nameLabel.text = documents[index].getFileName()
        docCell.viewButton.tag = index
        docCell.viewButton.addTarget(self, action: #selector(viewTapAction), for: .touchUpInside)
       return docCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let docCell = collectionView.dequeueReusableCell(withReuseIdentifier: "docCell", for: indexPath) as! DocumentCell
        let index = indexPath.row
        let desc = documents[index].getDescription()
        
        docCell.descLabel.text = desc
        
        //Dynamically get the individual description label height to change the collection view cell layout size
        let descLblHeight = Functions.calcHeightForView(text: desc, font: docCell.descLabel.font, width: layoutWidth)
        //Reset the height of the description label
        docCell.descLabel.heightAnchor.constraint(equalToConstant: descLblHeight).isActive = true
      
        return CGSize(width: layoutWidth, height: layoutHegith+descLblHeight)
    }
    
    @objc func viewTapAction(button: UIButton) {
        let index = button.tag
        let previewController = QLPreviewController()
        previewController.dataSource = self
        let fileName = documents[index].getFileName()
        let directoryURL: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.previewItem = directoryURL.appendingPathComponent(fileName)
        if QLPreviewController.canPreview(self.previewItem! as QLPreviewItem) {
            self.present(previewController, animated: true, completion: nil)
        } else {
            Functions.alert(vc: self, title: "Invalid", message: "This file cannot be opened.")
        }
    } 
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem! as QLPreviewItem
    }
}
