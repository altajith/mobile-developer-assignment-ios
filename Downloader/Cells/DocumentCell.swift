//
//  DocumentCell.swift
//  Downloader
//
//  Created by AJ on 1/10/20.
//  Copyright © 2020 AJ. All rights reserved.
//

import UIKit

class DocumentCell: UICollectionViewCell {
    
    let nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 18)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.text = "File name"
        return lbl
    }()
    
    let typeLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 18)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .darkGray
        lbl.text = "PDF"
        return lbl
    }()
    
    let descLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = Functions.font(size: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .darkGray
        lbl.text = "Description of file"
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 10
        return lbl
    }()
    
    let viewButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("VIEW", for: .normal)
        btn.setTitleColor(.systemBlue, for: .normal)
        btn.setTitleColor(.darkGray, for: .highlighted)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.contentHorizontalAlignment = .right
        btn.titleLabel?.font = Functions.font(size: 16)
        return btn
    }()
    
    let lineLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    func initUI() {
        backgroundColor = .white
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(nameLabel)
        addSubview(typeLabel)
        addSubview(descLabel)
        addSubview(viewButton)
        addSubview(lineLabel)
       
        initLayout()
    }
    
    private func initLayout() {
        NSLayoutConstraint.activate([
            //Name Label
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant:5),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant:5),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant:5),
            nameLabel.widthAnchor.constraint(equalTo: widthAnchor),
            
            //Type Label
            typeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant:10),
            typeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant:5),
            typeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant:5),
            typeLabel.widthAnchor.constraint(equalTo: widthAnchor),
            
            //Description Label
            descLabel.topAnchor.constraint(equalTo: typeLabel.bottomAnchor, constant:6),
            descLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            descLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant:5),
            descLabel.widthAnchor.constraint(equalTo: widthAnchor),
            
            //View Button
            viewButton.topAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            viewButton.leadingAnchor.constraint(equalTo: typeLabel.trailingAnchor, constant: -100),
            viewButton.widthAnchor.constraint(equalToConstant: 80),
            viewButton.heightAnchor.constraint(equalToConstant: 40),
            
            //Line
            lineLabel.topAnchor.constraint(equalTo: bottomAnchor),
            lineLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineLabel.heightAnchor.constraint(equalToConstant: 3),
            lineLabel.widthAnchor.constraint(equalTo: widthAnchor)
        ])
    }
    
    override var isHighlighted: Bool {
        didSet {
            //Change the collection view cell background color when the cell is tapped
            self.contentView.backgroundColor = isHighlighted ? .edededColor() : .white
        }
    }
}
